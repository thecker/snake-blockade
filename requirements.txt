pygame >= 2.4
moderngl >= 5.8
PyGLM >= 2.7
numpy >= 1.26
pygltflib >= 1.16
PyWavefront >= 1.3
moviepy >= 1.0