"""
Snake game with PC graphics mode evolution
"""

import json
from pathlib import Path
import argparse
import pygame
import moviepy.editor

from snake_history.data_management import GameDataManager
from snake_history.engine import GameEngine
from snake_history.rendering.gl_renderer import OpenGLRenderer

argparser = argparse.ArgumentParser()
argparser.add_argument('--windowed', action='store_true',
                       help='Run in windowed mode')
argparser.add_argument('--data_file', type=str, default='data/history.json',
                       help='Data file (JSON)')
argparser.add_argument('--no_overlay', action='store_true',
                       help='Do not use overlays (e.g. for scanlines)')
argparser.add_argument('--display', type=int, default=0, help='Display to use - usually 0 or 1')
argparser.add_argument('--unlimited_lives', action='store_true', default=False,
                       help='Unlimited lives')
argparser.add_argument('--skip_intro', action='store_true', help='Skip intro')
argparser.add_argument('--width', type=int, default=None, help='Screen width')
argparser.add_argument('--height', type=int, default=None, help='Screen height')
argparser.add_argument('--fps', type=int, default=10, help='Number of game frames per second')
argparser.add_argument('--verbosity_level', type=int, default=0, help='Verbosity level')
args = argparser.parse_args()
windowed = args.windowed
data_file = args.data_file
display_no = args.display
unlimited_lives = args.unlimited_lives
use_overlay = not args.no_overlay
play_intro = not args.skip_intro
width = args.width
height = args.height
fps = args.fps
verbosity_level = args.verbosity_level

data_folder = Path('data')
images_data_file = data_folder.joinpath('images/images.json')
if not images_data_file.exists():
    images_data_file = None
intro_data_file = data_folder.joinpath('intro/intro.json')
if intro_data_file.exists() and play_intro:
    # TODO: Not yet managed to run the intro via moviepy from the same pygame context. So we create a new one here,
    #  which we kill at the end of the intro
    pygame.init()
    size = pygame.display.get_desktop_sizes()[display_no]
    flags = pygame.FULLSCREEN | pygame.RESIZABLE
    surface = pygame.display.set_mode(size, flags=flags, display=display_no)
    with open(intro_data_file, 'r') as file:
        intro_config = json.loads(file.read())
    intro_filename = str(intro_data_file.parent.joinpath(intro_config['filename']))
    video = moviepy.editor.VideoFileClip(intro_filename, target_resolution=(size[1], size[0]))
    video.preview(fullscreen=True)
    pygame.quit()
gd_manager = GameDataManager(filename=Path(data_file), data_folder=data_folder, images_filename=images_data_file)
engine = GameEngine(gdm=gd_manager, game_fps=fps, unlimited_lives=unlimited_lives, verbosity=verbosity_level)
renderer = OpenGLRenderer(engine=engine, fps=30, windowed=windowed, use_overlay=use_overlay, display=display_no,
                          screen_width=width, screen_height=height)
engine.run()
