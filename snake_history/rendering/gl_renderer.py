"""OpenGL renderer module"""
import time
import os
import json
from pathlib import Path

import glm
import moderngl as mgl
import pygame as pg
import numpy as np
from pygame.locals import *

import snake_history.engine
from snake_history.rendering.base import Renderer
from snake_history.graphics import black, red
from snake_history.rendering.camera import Camera3d
from snake_history.rendering.mappers import GhostState, SpriteMapper, GhostSpriteMapper, Object3DMapper, Plane3DMapper
from snake_history.rendering.light import Light
from snake_history.rendering.model_import import WavefrontOBJImporter, GLTFImporter

SUPPORTED_IMAGE_FILE_EXT = [
    'png', 'jpg', 'jpeg'
]

SUPPORTED_OBJECT_FILE_EXT = [
    'obj', 'glb'
]


def get_texture(image: pg.Surface, ctx, flip_x=False, flip_y=False):
    texture_image = image.convert_alpha()
    # TODO: Seems like we need to flip the texture to get the UV map correctly placed
    #  Does not work for the "Sprites" now so we need to re-implement here...
    texture_image = pg.transform.flip(texture_image, flip_x=flip_x, flip_y=flip_y)
    texture = ctx.texture(
        size=texture_image.get_size(), components=4,
        data=pg.image.tostring(texture_image, 'RGBA'))
    # TODO: Should actually give access to the filtering algorithms
    #  to represent more modern styles
    texture.filter = (mgl.NEAREST, mgl.NEAREST)
    # # mipmaps
    # texture.filter = (mgl.LINEAR_MIPMAP_LINEAR, mgl.LINEAR)
    # texture.build_mipmaps()
    # # AF
    # texture.anisotropy = 32.0
    return texture, texture_image


class OpenGLRenderer(Renderer):

    def __init__(self, engine: snake_history.engine.GameEngine, **kwargs):
        super().__init__(engine=engine, **kwargs)
        self.DEFAULT_FONT = pg.font.SysFont('monaco', 32)
        pg.display.gl_set_attribute(pg.GL_CONTEXT_MAJOR_VERSION, 3)
        pg.display.gl_set_attribute(pg.GL_CONTEXT_MINOR_VERSION, 3)
        pg.display.gl_set_attribute(pg.GL_CONTEXT_PROFILE_MASK, pg.GL_CONTEXT_PROFILE_CORE)
        if self.windowed:
            self.flags = DOUBLEBUF | OPENGL | RESIZABLE # | SCALED
        else:
            # Seems like we need to add SCALED - otherwise the screen will be black
            # when starting in GL renderer
            self.flags = FULLSCREEN | DOUBLEBUF | OPENGL # | SCALED  # | RESIZABLE #
        self.screen = pg.display.set_mode(self.size, flags=self.flags, display=self.display)
        self.ctx = mgl.create_context()
        # TODO: Cannot using DEPTH_TEST - might hide the faces if we are only
        #  using 2D...
        # ctx_flags = mgl.BLEND # | mgl.DEPTH_TEST | mgl.CULL_FACE
        ctx_flags = mgl.CULL_FACE | mgl.BLEND | mgl.DEPTH_TEST
        self.ctx.enable(flags=ctx_flags)
        # self.ctx.enable(flags=mgl.BLEND)
        # TODO: Seems like blend_func is not implemented...
        self.ctx.blend_func = mgl.SRC_ALPHA, mgl.ONE_MINUS_SRC_ALPHA
        self.ctx.clear(0., 0., 0., 0.)
        self.camera = Camera3d(renderer=self)
        self.overlay_camera = Camera3d(renderer=self)
        self.overlay_camera.align_to_2d_view(width=self.size[0], height=self.size[1])
        game_style = self.engine.game_style
        # Set light before we create any mappers as they might use the light source on init
        # TODO: There seem to be some glitches with the shadows, if we place the light position (x,y) within the
        #  play field range...
        self.light = Light(position=(50, 50, -30), color=(1, 1., 0.8), ambient=0.2, diffuse=0.8, specular=1.)
        # For shadow mapping
        self.depth_texture = self.get_depth_texture()
        self.depth_fbo = self.ctx.framebuffer(depth_attachment=self.depth_texture)
        self.textures = {}
        self.texture_images = {}
        self.load_textures()
        self.vbos = {}
        self.load_models()
        # TODO: We need to make sure to use different texture IDs for the
        #  textures, which are active at the same time
        self.player_actor = SpriteMapper(
            renderer=self, image_filename=game_style.snake_head, tex_id=0)
        self.food_actor = SpriteMapper(
            renderer=self, image_filename=game_style.food_shape, tex_id=1)
        self.background = None
        self.snake_body_actors = self.create_body_actors()
        self.ghost_actors = []
        self.overlay = None
        self.score_board = self.create_score_board()
        self.floor = None
        self.image_overlay = None
        if self.engine.verbosity > 1:
            print(pg.display.Info())
            print(pg.display.get_driver())
            print(pg.display.list_modes(depth=0, flags=FULLSCREEN, display=self.display))

    @property
    def delta_time(self):
        return 1 / self.fps

    @property
    def actors(self):
        actors = [self.player_actor, self.food_actor] + self.snake_body_actors
        if self.floor:
            actors.append(self.floor)
        return actors

    def get_depth_texture(self):
        depth_texture = self.ctx.depth_texture(self.size)
        return depth_texture

    def load_image_to_texture(self, filename, name):
        print(f'Loading image {filename}...')
        image_surface = pg.image.load(filename).convert_alpha()
        texture, texture_image = get_texture(image=image_surface, ctx=self.ctx)
        self.textures[name] = texture
        self.texture_images[name] = texture_image

    def load_textures(self):
        data_path = Path('data')
        # We only want the images to be loaded, which are actually referenced
        images_path = data_path.joinpath('images')
        if images_path.exists():
            images_config = images_path.joinpath('images.json')
            if images_config.exists():
                with open(images_config, 'r') as file:
                    image_data = json.loads(file.read())
                image_filenames = [image['filename'] for image in image_data['images']]
                for image_filename in image_filenames:
                    filepath = images_path.joinpath(image_filename)
                    self.load_image_to_texture(filename=filepath, name=image_filename)
        # Further texture data dirs
        # TODO: Should probably get that from game data manager like for the "images" above
        data_dirs = ['3d', 'overlays', 'sprites', 'tiles']
        for data_dir in data_dirs:
            for root, dirs, files in os.walk(data_path.joinpath(data_dir)):
                for name in files:
                    if name.split('.')[-1].lower() in SUPPORTED_IMAGE_FILE_EXT:
                        filename = os.path.join(root, name)
                        self.load_image_to_texture(filename=filename, name=name)

    def add_texture(self, name, image_surface):
        texture, texture_image = get_texture(image=image_surface, ctx=self.ctx)
        self.textures[name] = texture
        self.texture_images[name] = texture_image

    def load_models(self):
        data_path = 'data/3d'
        model_count = 0
        for root, dirs, files in os.walk(data_path, topdown=True):
            for name in files:
                if name.split('.')[-1].lower() in SUPPORTED_OBJECT_FILE_EXT:
                    filename = os.path.join(root, name)
                    print(f'Loading object {filename}...')
                    if name.endswith('.obj'):
                        vertex_data = WavefrontOBJImporter.import_model(filename=filename)
                    elif name.endswith('.glb'):
                        vertex_data = GLTFImporter.import_model(filename=filename, load_tangent=True)
                    else:
                        extension = name.split('.')[-1]
                        raise NotImplemented(f'Extension {extension} not implemented yet!')
                    vbo = self.ctx.buffer(vertex_data)
                    self.vbos[name] = vbo
                    model_count += 1
        print(f'{model_count} models loaded.')

    def create_body_actors(self):
        player = self.engine.player
        game_style = self.engine.game_style
        body_actors = []
        for i in range(len(player.body) - 1):
            if game_style.use_3d:
                if game_style.normal_mapping:
                    normal_map_file = 'snake_body_normal_map.png'
                else:
                    normal_map_file = None
                body_actor = Object3DMapper(
                    renderer=self, obj_filename=game_style.body_model,
                    texture_filename=game_style.snake_shape,
                    normal_map_filename=normal_map_file,
                    tex_id=3, initial_rot=(-90., 0., 0.), centered=True
                )
            else:
                body_actor = SpriteMapper(
                    renderer=self, image_filename=game_style.snake_shape, tex_id=3)
            body_actors.append(body_actor)
        return body_actors

    def create_score_board(self, choice=1, font_color=black, style_name=None):
        # TODO: Not sure, if this is the most efficient way to display
        #  text...
        score = self.engine.score
        score_str = f'Score: {score}'
        if style_name:
            score_str = score_str + f' ({style_name})'
        score_surface = self.DEFAULT_FONT.render(score_str, True, font_color)
        texture_name = f'score_board_{score}'
        self.add_texture(name=texture_name, image_surface=score_surface)
        score_board = SpriteMapper(
            renderer=self, image_filename=texture_name, camera=self.overlay_camera,
            tex_id=5
        )
        return score_board

    def create_caption(self, caption_text, font_size=64, font_color=red):
        # TODO: Not sure, if this is the most efficient way to display
        #  text...
        font = pg.font.SysFont('monaco', font_size)
        font_surface = font.render(caption_text, True, font_color)
        texture_name = f'image_caption_{caption_text}'
        self.add_texture(name=texture_name, image_surface=font_surface)
        caption = SpriteMapper(
            renderer=self, image_filename=texture_name, camera=self.overlay_camera,
            tex_id=5
        )
        return caption

    def create_background_image(self)->pg.Surface:
        #TODO: Not 100% safe - we assume that, if tiles are defined, the
        #  the block size is 0.5 * tile size...
        game_style = self.engine.game_style
        width = game_style.width
        height = game_style.height
        block_size_x = game_style.block_size_x
        block_size_y = game_style.block_size_y
        screen_width = width * block_size_x
        screen_height = height * block_size_y
        bg_surface = pg.Surface((screen_width, screen_height))
        bg_surface.fill(color=game_style.bg_color)
        #TODO: Currently expect, that all tiles have the same size
        floor_tile_filename = game_style.floor_tile
        floor_tile = self.texture_images.get(floor_tile_filename)
        if floor_tile:
            tile_width, tile_height = floor_tile.get_size()
            # Make sure we fill up "half" rows as well
            no_tiles_x = width
            no_tiles_y = height
            # Fill floor
            for i in range(no_tiles_x - 2):
                x_pos = (i + 1) * tile_width
                # First line is score, 2nd + last line wall
                for j in range(no_tiles_y - 3):
                    y_pos = (j + 2) * tile_height
                    bg_surface.blit(source=floor_tile, dest=(x_pos, y_pos))
        wall_tile_filename = game_style.wall_tile
        wall_tile = self.texture_images.get(wall_tile_filename)
        wall_corner_tile_filename = game_style.wall_corner_tile
        wall_corner_tile = self.texture_images.get(wall_corner_tile_filename)
        if wall_tile and wall_corner_tile:
            # Fill walls
            wall_tile_left = pg.transform.rotate(wall_tile, 180.)
            wall_tile_up = pg.transform.rotate(wall_tile, 90.)
            wall_tile_down = pg.transform.rotate(wall_tile, 270.)
            for i in range(no_tiles_x - 2):
                x_pos = (i + 1) * tile_width
                bg_surface.blit(source=wall_tile,
                                dest=(x_pos, tile_height))
                bg_surface.blit(source=wall_tile_left,
                                dest=(x_pos, height * block_size_y - tile_height))
            for i in range(no_tiles_y - 3):
                y_pos = (i + 2) * tile_height
                bg_surface.blit(source=wall_tile_down,
                                dest=(width * block_size_x - tile_width, y_pos))
                bg_surface.blit(source=wall_tile_up, dest=(0, y_pos))
            # Fill corners
            tile_width, tile_height = wall_tile.get_size()
            bg_surface.blit(source=wall_corner_tile, 
                            dest=(0, tile_height))
            bg_surface.blit(source=pg.transform.rotate(wall_corner_tile, 270.),
                            dest=(screen_width - tile_width, tile_height))
            bg_surface.blit(source=pg.transform.rotate(wall_corner_tile, 180.),
                            dest=(screen_width - tile_width, screen_height - tile_height))
            bg_surface.blit(source=pg.transform.rotate(wall_corner_tile, 90.), 
                            dest=(0, screen_height - tile_height))
        else:
            generic_tile = pg.Surface((block_size_x, block_size_y))
            generic_tile.fill(color=game_style.font_color)
            for i in range(width):
                x_pos = i * block_size_x
                bg_surface.blit(source=generic_tile, 
                                dest=(x_pos, 2 * block_size_y))
                bg_surface.blit(source=generic_tile, 
                                dest=(x_pos, height * block_size_y - block_size_y)) 
            for i in range(0, height - 3):
                y_pos = (i + 3) * block_size_y
                bg_surface.blit(source=generic_tile, dest=(0, y_pos))
                bg_surface.blit(source=generic_tile, 
                                dest=(width * block_size_x - block_size_x, y_pos))
        return bg_surface

    def update_body_actors(self):
        game_style = self.engine.game_style
        snake_positions = self.engine.player.body
        snake_body_actors = self.snake_body_actors
        block_size_x = game_style.block_size_x
        block_size_y = game_style.block_size_y
        if len(snake_body_actors) < len(snake_positions) - 1:
            while len(snake_body_actors) < len(snake_positions) - 1:
                if game_style.use_3d:
                    if game_style.normal_mapping:
                        body_normal_map_file = 'snake_body_normal_map.png'
                    else:
                        body_normal_map_file = None
                    body_actor = Object3DMapper(
                        renderer=self,
                        obj_filename=game_style.body_model,
                        texture_filename=game_style.snake_shape,
                        normal_map_filename=body_normal_map_file,
                        # texture_filename=game_style.snake_body_texture,
                        tex_id=3, initial_rot=(-90., 0., 0.), centered=True
                    )
                else:
                    body_actor = SpriteMapper(
                        renderer=self, image_filename=game_style.snake_shape, tex_id=3)
                snake_body_actors.append(body_actor)
        for i, body_actor in enumerate(snake_body_actors):
            pos = snake_positions[i + 1]
            body_actor.set_transform(x_pos=pos[0] * block_size_x, y_pos=pos[1] * block_size_y, angle=0.)

    def draw_ghost_sprites(self):
        for i, sprite in enumerate(self.ghost_actors):
            if sprite.state == GhostState.ALIVE:
                sprite.update_alpha()
                sprite.render()
            else:
                if self.engine.verbosity >= 3:
                    print(f'Ghost sprite no {i} expired.')
                self.ghost_actors.pop(i)
                sprite.destroy()

    def render_shadow(self):
        self.depth_fbo.clear()
        self.depth_fbo.use()
        for obj in self.actors:
            # TODO: I guess we also need to have a render shadow in the Plane3DMapper - as this should actually
            #  be the object receiving the shadows
            if isinstance(obj, Object3DMapper):
                obj.render_shadow()

    def render(self):
        render_start = time.time()
        self.ctx.clear(color=(0.0, 0.0, 0.0))
        game_style = self.engine.game_style
        player = self.engine.player
        block_size_x = game_style.block_size_x
        block_size_y = game_style.block_size_y
        x_pos = player.x_pos * block_size_x
        y_pos = player.y_pos * block_size_y
        direction = player.direction
        if game_style.rotate_head:
            if direction == 'RIGHT':
                angle = 0.
            elif direction == 'LEFT':
                angle = 180.
            elif direction == 'UP':
                angle = -90.
            elif direction == 'DOWN':
                angle = 90.
        else:
            angle = 0.
        self.player_actor.set_transform(x_pos=x_pos, y_pos=y_pos, angle=glm.radians(angle))
        # Food
        food = self.engine.food
        food_x_pos = food.x_pos * block_size_x
        food_y_pos = food.y_pos * block_size_y
        self.food_actor.set_transform(x_pos=food_x_pos, y_pos=food_y_pos)
        self.update_body_actors()

        # We need to update the score board in each game step (or 
        # at least every time the score changes)
        self.score_board.destroy()
        self.score_board = self.create_score_board(
            font_color=game_style.font_color, style_name=game_style.name
        )
        if game_style.ghosting_time and not game_style.use_3d:
            if game_style.ghosting_time:
                food_ghost = GhostSpriteMapper(
                    sprite=self.food_actor,
                    ghosting_time=game_style.ghosting_time,
                    strength=game_style.ghosting_strength)
                self.ghost_actors.append(food_ghost)
                if self.engine.verbosity >= 3:
                    print(f'Created ghost sprite for food at {food_ghost.x_pos}, {food_ghost.y_pos}')
                head_ghost = GhostSpriteMapper(
                    sprite=self.player_actor, ghosting_time=game_style.ghosting_time,
                    strength=game_style.ghosting_strength
                )
                for snake_body_actor in self.snake_body_actors:
                    body_ghost = GhostSpriteMapper(
                        sprite=snake_body_actor, ghosting_time=game_style.ghosting_time,
                        strength=game_style.ghosting_strength
                    )
                    self.ghost_actors.append(body_ghost)
                self.ghost_actors.append(head_ghost)
            for i in range(int(self.fps / self.engine.game_fps)):
                # TODO: There seem to be some glitches when we are using this for 3d modes...
                if self.background:
                    self.background.render()
                if self.floor:
                    self.floor.render()
                self.player_actor.render()
                for body_sprite in self.snake_body_actors:
                    body_sprite.render()
                if game_style.use_3d:
                    self.food_actor.rotate(angle=np.deg2rad(2.))
                self.food_actor.render()
                self.score_board.render()
                if self.ghost_actors:
                    self.draw_ghost_sprites()
                if self.overlay:
                    self.overlay.render()
                pg.display.flip()
                self.fpsController.tick(self.fps)
        else:
            # Render
            # TODO: Should make `render_shadow` an optional flag in game style / renderer
            if game_style.use_3d:
                self.render_shadow()
                self.ctx.screen.use()
            if self.background:
                self.background.render()
            if self.floor:
                self.floor.render()
            self.player_actor.render()
            for body_sprite in self.snake_body_actors:
                body_sprite.render()
            if game_style.use_3d:
                self.food_actor.rotate(angle=np.deg2rad(3.))
            self.food_actor.render()
            self.score_board.render()
            if self.overlay:
                self.overlay.render()
            pg.display.flip()
        if self.engine.verbosity > 1:
            print(f'Render took {time.time() - render_start}s.')

    def create_floor(self):
        game_style = self.engine.game_style
        floor_image = game_style.floor_tile
        width = game_style.width
        height = game_style.height
        # Note that current floor tiles represent two game blocks
        floor_mapper = Plane3DMapper(renderer=self, image_filename=floor_image, size=(width, height),
                                     repeat=(width / 2, height / 2), tex_id=7)
        floor_mapper.set_transform(x_pos=width / 2, y_pos=height / 2, z_pos=1.)
        return floor_mapper

    def reset(self):
        game_style = self.engine.game_style
        # Clean-up
        self.player_actor.destroy()
        self.food_actor.destroy()
        if self.floor:
            self.floor.destroy()
        if self.background:
            self.background.destroy()
        if self.overlay:
            self.overlay.destroy()
        for i in range(len(self.snake_body_actors)):
            snake_body_actor = self.snake_body_actors.pop(0)
            snake_body_actor.destroy()
        for i in range(len(self.ghost_actors)):
            ghost_actor = self.ghost_actors.pop(0)
            ghost_actor.destroy()
        # Adjust viewport
        aspect_ratio = game_style.aspect_ratio
        scaled_width = int(aspect_ratio * self.size[1])
        x_offset = int((self.size[0] - scaled_width) / 2)
        self.ctx.viewport = (x_offset, 0, scaled_width, self.size[1])
        # Adjust overlay camera
        self.overlay_camera.align_to_2d_view(width=scaled_width, height=self.size[1])
        # Create new actors
        if game_style.use_3d:
            # Make sure to enable CULL_FACE and DEPTH_TEST
            ctx_flags = mgl.CULL_FACE | mgl.BLEND | mgl.DEPTH_TEST
            self.ctx.enable(flags=ctx_flags)
            # Update "field of view"
            fov = 50.
            z_dist = (game_style.height / 2) / np.tan(np.deg2rad(fov / 2))
            center_x = game_style.width / 2
            center_y = game_style.height / 2
            # Note, that we need z < 0 and set the up axis to negative y-axis to match the "2d space"
            # definition, which sets x=0,y=0 at the top left corner of the screen
            eye = (center_x,  game_style.height + center_y, -z_dist)
            look_at = (center_x, center_y, 0)
            # TODO: We should rather update the camera than creating an new one and we need to under stand how to
            #   use aspect ratio correctly for different screen modes... e.g. aspect_ratio=4/3*16/9
            self.camera = Camera3d(
                renderer=self, eye=eye, look_at=look_at, up=(0, 0, -1), fov=fov, aspect_ratio=aspect_ratio)
            # Need to add some transformations to get the wavefront objects into the correct alignment
            if game_style.normal_mapping:
                normal_map_file = 'snake_head_normal_map.png'
                food_normal_map_file = 'food_normal_map.png'
            else:
                normal_map_file = None
                food_normal_map_file = None
            self.player_actor = Object3DMapper(
                renderer=self, obj_filename=game_style.head_model,
                texture_filename=game_style.snake_head,
                normal_map_filename=normal_map_file,
                tex_id=0, initial_rot=(-90., 0., 0.), centered=True
            )
            self.player_actor.set_rotation_axis(glm.vec3(0, -1, 0))
            self.food_actor = Object3DMapper(
                renderer=self,
                obj_filename=game_style.food_model,
                texture_filename=game_style.food_shape,
                normal_map_filename=food_normal_map_file,
                tex_id=1, initial_rot=(-90., 0., 0.), centered=True
            )
            self.food_actor.set_rotation_axis(glm.vec3(0, -1, 0))
            if game_style.floor_tile:
                self.floor = self.create_floor()
            else:
                self.floor = None
            self.background = None
            # TODO: In 3d we will actually need to add walls (and eventually a skybox)
        else:
            # 2D style
            # Make sure we disable DEPTH_TEST - otherwise sprites don't show up
            ctx_flags = mgl.DEPTH_TEST
            self.ctx.disable(flags=ctx_flags)
            self.camera.align_to_2d_view(width=game_style.resolution[0], height=game_style.resolution[1])
            self.player_actor = SpriteMapper(
                renderer=self, image_filename=game_style.snake_head, tex_id=0)
            self.food_actor = SpriteMapper(
                renderer=self, image_filename=game_style.food_shape, tex_id=1)
            # TODO: Add support for backgrounds with preloaded images again
            bg_image = self.create_background_image()
            bg_image_name = f'bg_image_{self.engine.game_style.name}'
            self.add_texture(name=bg_image_name, image_surface=bg_image)
            self.background = SpriteMapper(renderer=self, image_filename=bg_image_name, tex_id=2)
            self.floor = None
        if game_style.overlay and self.use_overlay:
            self.overlay = SpriteMapper(
                renderer=self, image_filename=game_style.overlay, camera=self.overlay_camera,
                tex_id=6
            )
        else:
            self.overlay = None
        self.snake_body_actors = self.create_body_actors()

    def show_image(self, image_filename, caption=None):
        # TODO: Need to harmonize - we are actually getting the full path here from the engine...
        image_surf = self.texture_images[str(image_filename)]
        im_width, im_height = image_surf.get_size()
        _, _, screen_width, screen_height = self.ctx.viewport
        scale_width = screen_width / im_width
        scale_height = screen_height / im_height
        if scale_width > scale_height:
            scale_factor = scale_height
        else:
            scale_factor = scale_width
        offset_x = (screen_width - im_width * scale_factor) / 2
        offset_y = (screen_height - im_height * scale_factor) / 2
        if self.image_overlay:
            self.image_overlay.destroy()
        self.image_overlay = SpriteMapper(renderer=self, image_filename=image_filename, tex_id=4,
                                          scale=(scale_factor, scale_factor, 1),
                                          camera=self.overlay_camera)
        self.image_overlay.set_transform(x_pos=offset_x, y_pos=offset_y, z_pos=0.01)
        self.image_overlay.render()
        if caption:
            caption_overlay = self.create_caption(caption_text=caption)
            if self.engine.verbosity >= 1:
                print(f'Caption: {caption}')
            caption_width, caption_height = caption_overlay.texture_image.get_size()
            # Make caption centered at bottom of image
            caption_offset_x = (screen_width - caption_width) / 2
            caption_offset_y = offset_y + im_height * scale_factor - caption_height
            # Need to make sure, this is rendered in front of the image in 3d projection (hence add some z_pos)
            caption_overlay.set_transform(x_pos=caption_offset_x, y_pos=caption_offset_y, z_pos=0.1)
            caption_overlay.render()
        pg.display.flip()

    def hide_current_image(self):
        if self.image_overlay:
            self.image_overlay.destroy()

    def show_centered_caption(self, text='Press any key to continue'):
        caption_overlay = self.create_caption(caption_text=text)
        caption_width, caption_height = caption_overlay.texture_image.get_size()
        # Make caption centered on screen
        _, _, screen_width, screen_height = self.ctx.viewport
        caption_offset_x = (screen_width - caption_width) / 2
        caption_offset_y = (screen_height - caption_height) / 2
        # Need to make sure, this is rendered in front of the image in 3d projection (hence add some z_pos)
        caption_overlay.set_transform(x_pos=caption_offset_x, y_pos=caption_offset_y, z_pos=0.1)
        caption_overlay.render()
        pg.display.flip()

    def gameOver(self):
        self.player_actor.destroy()
        self.food_actor.destroy()
        self.score_board.destroy()
        if self.background:
            self.background.destroy()
        if self.floor:
            self.floor.destroy()
        if self.overlay:
            self.overlay.destroy()
        for i in range(len(self.snake_body_actors)):
            sprite = self.snake_body_actors.pop(0)
            sprite.destroy()
        for i in range(len(self.ghost_actors)):
            sprite = self.ghost_actors.pop(0)
            sprite.destroy()
        self.depth_fbo.release()
        for key in list(self.textures.keys()):
            texture = self.textures.pop(key)
            texture.release()
        for key in list(self.vbos.keys()):
            vbo = self.vbos.pop(key)
            vbo.release()
