"""Mappers map objects (sprites, 3d objects) to the view port"""
import abc
import os
import time
from enum import IntEnum
from pathlib import Path

import glm
import numpy as np


class GhostState(IntEnum):
    ALIVE = 0

    EXPIRED = 1


class Mapper:
    shader_name: str
    # vbo_format is e.g. '2f 3f 3f'
    vbo_format: str
    # vbo attribs - e.g. 'in_texcoord_0', 'in_normal', 'in_position'
    vbo_attribs: list[str]

    def __init__(self, renderer, initial_rot=(0., 0., 0), scale=(1, 1, 1), centered=False, camera=None,
                 custom_shader_name=None, custom_vbo_format=None, custom_vbo_attribs=None
                 ):
        """Base class for mappers

        Args:
            renderer: OpenGL renderer
            initial_rot: Initial rotation
            scale: Scale
            centered: Origin centered - if False origin is expected at left top corner
            camera: Custom camera - defaults to camera of renderer if None
        """
        self.custom_shader_name = custom_shader_name
        self.custom_vbo_format = custom_vbo_format
        self.custom_vbo_attribs = custom_vbo_attribs
        self.renderer = renderer
        self.ctx = renderer.ctx
        self.centered = centered
        if camera is None:
            camera = self.renderer.camera
        self.camera = camera
        self._angle = 0.
        self._x_pos = 0.
        self._y_pos = 0.
        self._z_pos = 0.
        self.initial_rot = glm.vec3([glm.radians(a) for a in initial_rot])
        self.scale = scale
        self._rotation_axis = glm.vec3(0., 0., 1.)
        self.vbo = self.get_vbo()
        if self.custom_shader_name:
            shader_name = self.custom_shader_name
        else:
            shader_name = self.shader_name
        self.shader_program = self.get_shader_program(shader_name)
        self.m_model = glm.mat4()
        self.vao = self.get_vao()
        self.on_init()

    @property
    def x_pos(self) -> float:
        return self._x_pos

    @property
    def y_pos(self) -> float:
        return self._y_pos

    @property
    def angle(self) -> float:
        return self._angle

    @property
    @abc.abstractmethod
    def size_xy(self) -> tuple[float, float]:
        """Size in x- and y-dimension"""

    @abc.abstractmethod
    def on_init(self):
        """Perform some tasks on init"""

    @abc.abstractmethod
    def get_vertex_data(self) -> np.array:
        """Get the vertex data"""

    def get_vbo(self):
        """Get vertex buffer object"""
        vertex_data = self.get_vertex_data()
        vbo = self.ctx.buffer(vertex_data)
        return vbo

    def get_vao(self):
        """Get vertex array object"""
        if self.custom_vbo_format:
            vbo_format = self.custom_vbo_format
        else:
            vbo_format = self.vbo_format
        if self.custom_vbo_attribs:
            vbo_attribs = self.custom_vbo_attribs
        else:
            vbo_attribs = self.vbo_attribs
        vao = self.ctx.vertex_array(
            self.shader_program,
            [(self.vbo, vbo_format, *vbo_attribs)])
        return vao

    def get_shader_program(self, shader_name):
        shader_folder = Path(os.path.dirname(os.path.abspath(__file__))).joinpath('shaders')
        with open(shader_folder.joinpath(f'{shader_name}.vert'), 'r') as file:
            vertex_shader = file.read()

        with open(shader_folder.joinpath(f'{shader_name}.frag'), 'r') as file:
            fragment_shader = file.read()
        program = self.ctx.program(vertex_shader=vertex_shader,
                                   fragment_shader=fragment_shader)
        return program

    def render(self):
        self.vao.render()

    def set_rotation_axis(self, vector=glm.vec3(0, 0, 1)):
        self._rotation_axis = vector

    def set_transform(self, x_pos, y_pos, z_pos=None, angle=None):
        self._x_pos = x_pos
        self._y_pos = y_pos
        if angle is None:
            angle = self._angle
        self._angle = angle
        if z_pos is None:
            z_pos = self._z_pos
        self._z_pos = z_pos
        m_model = glm.mat4()
        # Translate
        m_model = glm.translate(m_model, glm.vec3(x_pos, y_pos, z_pos))

        # Rotation part
        size_x, size_y = self.size_xy
        if not self.centered:
            # Translate to center position
            m_model = glm.translate(m_model, glm.vec3(size_x / 2, size_y / 2, 0.))
        # Initial rotation
        m_model = glm.rotate(m_model, self.initial_rot.z, glm.vec3(0, 0, 1))
        m_model = glm.rotate(m_model, self.initial_rot.y, glm.vec3(0, 1, 0))
        m_model = glm.rotate(m_model, self.initial_rot.x, glm.vec3(1, 0, 0))
        m_model = glm.rotate(m_model, angle, self._rotation_axis)

        if not self.centered:
            # Translation back from rotation center
            m_model = glm.translate(m_model, glm.vec3(-size_x / 2, -size_y / 2, 0.))
        # Scale
        self.m_model = glm.scale(m_model, self.scale)
        self.shader_program['m_model'].write(self.m_model)

    def translate(self, dx, dy):
        """Relative translation"""
        self._x_pos += dx
        self._y_pos += dy
        self.m_model = glm.translate(self.m_model, glm.vec3(dx, dy, 0))
        self.shader_program['m_model'].write(self.m_model)

    def rotate(self, angle):
        """Relative rotation"""
        # Note, that we need to translate to and fro from the center to get
        # rotation around the center not the top left vertex
        self._angle += angle
        m_model = self.m_model
        if not self.centered:
            size_x, size_y = self.size_xy
            m_model = glm.translate(m_model, glm.vec3(size_x / 2, size_y / 2, 0.))
        m_model = glm.rotate(m_model, angle, self._rotation_axis)
        if not self.centered:
            size_x, size_y = self.size_xy
            self.m_model = glm.translate(m_model, glm.vec3(-size_x / 2, -size_y / 2, 0.))
        self.shader_program['m_model'].write(self.m_model)

    def update_camera(self):
        self.shader_program['m_proj'].write(self.camera.m_proj)

    def destroy(self):
        self.vbo.release()
        self.shader_program.release()
        self.vao.release()


class SpriteMapper(Mapper):
    shader_name = 'sprite'
    vbo_format = '2f 2f'
    vbo_attribs = ['in_texcoord_0', 'in_position']

    def __init__(self, renderer, image_filename, tex_id=0, **kwargs):
        self.image_filename = image_filename
        self.tex_id = tex_id
        self.texture = renderer.textures[image_filename]
        self.texture_image = renderer.texture_images[image_filename]
        super().__init__(renderer=renderer, **kwargs)

    def on_init(self):
        self.shader_program['u_texture_0'] = self.tex_id
        self.texture.use(location=self.tex_id)
        self.shader_program['m_proj'].write(self.camera.m_proj)
        self.shader_program['m_model'].write(self.m_model)

    @property
    def size_xy(self) -> tuple[float, float]:
        return self.texture_image.get_size()

    def destroy(self):
        self.vbo.release()
        self.shader_program.release()
        self.vao.release()

    def get_vertex_data(self):
        size_x, size_y = self.texture_image.get_size()
        vertices = [(0, 0), (0, size_y), (size_x, size_y), (size_x, 0)]
        tri_indices = [(0, 1, 3), (3, 1, 2)]
        vertex_data = self.get_data(vertices, tri_indices)
        tex_coord = [(0, 0), (0, 1), (1, 1), (1, 0)]
        tex_coord_indices = [(0, 1, 3), (3, 1, 2)]
        tex_coord_data = self.get_data(tex_coord, tex_coord_indices)
        vertex_data = np.hstack([tex_coord_data, vertex_data])
        return vertex_data

    @staticmethod
    def get_data(vertices, indices):
        data = [vertices[ind] for triangle in indices for ind in triangle]
        return np.array(data, dtype='f4')


class GhostSpriteMapper(SpriteMapper):

    shader_name = 'ghost_sprite'

    def __init__(self, sprite: SpriteMapper, ghosting_time=1.0, strength=1.0, **kwargs):
        self.ghosting_time = ghosting_time
        self.strength = strength
        self.creation_time = time.time()
        super().__init__(renderer=sprite.renderer, image_filename=sprite.image_filename,
                         tex_id=sprite.tex_id, **kwargs)
        self.set_transform(x_pos=sprite.x_pos, y_pos=sprite.y_pos, angle=sprite.angle)

    def on_init(self):
        self.shader_program['u_texture_0'] = self.tex_id
        self.shader_program['u_alpha'] = 0.5
        self.texture.use(location=self.tex_id)
        self.shader_program['m_proj'].write(self.renderer.camera.m_proj)
        self.shader_program['m_model'].write(self.m_model)

    def update_alpha(self):
        alpha = (1.0 - min([(time.time() - self.creation_time) / self.ghosting_time, 1.0])) * self.strength
        if self.renderer.engine.verbosity >= 3:
            print(f'Setting new alpha for ghost to {alpha}')
        self.shader_program['u_alpha'] = alpha

    @property
    def state(self):
        if time.time() - self.creation_time < self.ghosting_time:
            state = GhostState.ALIVE
        else:
            state = GhostState.EXPIRED
        return state


class Object3DMapper(Mapper):

    shader_name = 'object3d'
    vbo_format = '2f 3f 3f 4f'
    vbo_attribs = ['in_texcoord_0', 'in_normal', 'in_position', 'in_tangent']

    def __init__(self, renderer, obj_filename, texture_filename, tex_id=0, normal_map_filename=None, **kwargs):
        self.obj_filename = obj_filename
        self.tex_id = tex_id
        self.texture = renderer.textures[texture_filename]
        self.texture_image = renderer.texture_images[texture_filename]
        # TODO: Normal map currently not supported with OBJ files as we
        #  need tangent information in model
        if normal_map_filename and not self.obj_filename.endswith('.obj'):
            custom_shader_name = 'object3d_nmap'
            custom_vbo_format = '2f 3f 3f 4f'
            custom_vbo_attribs = ['in_texcoord_0', 'in_normal', 'in_position', 'in_tangent']
            self.normal_map = renderer.textures[normal_map_filename]
            self.normal_map_image = renderer.texture_images[normal_map_filename]
        else:
            self.normal_map = None
            self.normal_map_image = None
            custom_shader_name = None
            custom_vbo_format = None
            custom_vbo_attribs = None

        self.depth_texture = renderer.depth_texture
        # TODO: Actually need to fix some program structure - self.ctx is usually created in super().__init__,
        #  but we need it for calling self.get_shader_program. We can however not put call to self.get_shader...
        #  after the super().__init__ as it calls on_init, where the self.shadow_shader_program is required
        self.ctx = renderer.ctx
        self.shadow_shader_program = self.get_shader_program(shader_name='shadow_map')
        super().__init__(renderer=renderer, custom_shader_name=custom_shader_name,
                         custom_vbo_format=custom_vbo_format, custom_vbo_attribs=custom_vbo_attribs, **kwargs)
        # Need to call after super().__init__ as we need access to some properties set there custom_vbo...
        self.shadow_vao = self.get_shadow_vao()

    def on_init(self):
        self.shader_program['m_view_light'].write(self.renderer.light.m_view_light)
        # depth texture
        self.shader_program['shadowMap'] = 32
        # Note that we use th same texture for objects
        self.depth_texture.use(location=32)

        # shadows
        self.shadow_shader_program['m_proj'].write(self.camera.m_proj)
        self.shadow_shader_program['m_view_light'].write(self.renderer.light.m_view_light)
        self.shadow_shader_program['m_model'].write(self.m_model)

        self.shader_program['u_texture_0'] = self.tex_id
        self.texture.use(location=self.tex_id)
        if self.normal_map:
            # TODO: Seems like there is a limit for the layout IDs around 96 - if we go beyond we get the
            #  normal map as texture... Should implement some more sophisticated handling for the textures
            self.shader_program['u_normal_map'] = self.tex_id + 64
            self.normal_map.use(location=self.tex_id + 64)
        self.shader_program['m_proj'].write(self.camera.m_proj)
        self.shader_program['m_model'].write(self.m_model)
        self.shader_program['m_view'].write(self.camera.m_view)
        self.shader_program['camPos'].write(self.camera.eye)
        # self.shader_program['m_view_light'].write(self.renderer.light.m_view_light)
        self.shader_program['light.position'].write(self.renderer.light.position)
        self.shader_program['light.Ia'].write(self.renderer.light.Ia)
        self.shader_program['light.Id'].write(self.renderer.light.Id)
        self.shader_program['light.Is'].write(self.renderer.light.Is)

    def get_shadow_vao(self):
        """Get vertex array object for shadows"""
        if self.custom_vbo_format:
            vbo_format = self.custom_vbo_format
        else:
            vbo_format = self.vbo_format
        if self.custom_vbo_attribs:
            vbo_attribs = self.custom_vbo_attribs
        else:
            vbo_attribs = self.vbo_attribs
        # TODO: Do we really need normals, tangents etc. for the shadow shader
        vao = self.ctx.vertex_array(
            self.shadow_shader_program,
            [(self.vbo, vbo_format, *vbo_attribs)])
        return vao

    def update_shadow(self):
        self.shadow_shader_program['m_model'].write(self.m_model)

    def render_shadow(self):
        self.update_shadow()
        self.shadow_vao.render()

    def get_vbo(self):
        # We use a VBO here, which is already loaded by the renderer
        vbo = self.renderer.vbos[self.obj_filename]
        return vbo

    def get_vertex_data(self) -> np.array:
        pass

    @property
    def size_xy(self) -> tuple[float, float]:
        # TODO: Need to get the size of the object
        return self.scale[0], self.scale[1]

    def update_camera(self):
        self.texture.use(location=self.tex_id)
        self.shader_program['m_proj'].write(self.camera.m_proj)
        self.shader_program['m_view'].write(self.camera.m_view)
        self.shader_program['camPos'].write(self.camera.eye)

    def destroy(self):
        # Note, that since the VBO is taken from the renderer's VBO lib, we do
        # not want to interfere with the garbage collection (should be taken care
        # of by the renderer)
        self.shader_program.release()
        self.vao.release()


class Plane3DMapper(Mapper):
    # TODO: Should add a base class for "texture mapper"
    # TODO: Some code out-commented regarding shadow mapping - only required, if the plane should cast
    #  shadows
    """Mapper which draws a texture repeatedly in u and v dimension of a plane"""

    shader_name = 'plane3d'
    vbo_format = '2f 3f'
    vbo_attribs = ['in_texcoord_0', 'in_position']

    # TODO: Should harmonize weather we provide textures as pg.Surface or filenames
    def __init__(self, renderer, image_filename, tex_id=0, size=(1., 1.), repeat=(1, 1), **kwargs):
        self.image_filename = image_filename
        self.tex_id = tex_id
        self.size = size
        self.repeat = repeat
        self.texture = renderer.textures[image_filename]
        self.texture_image = renderer.texture_images[image_filename]
        self.depth_texture = renderer.depth_texture
        # self.ctx = renderer.ctx
        # self.shadow_shader_program = self.get_shader_program(shader_name='shadow_map')
        super().__init__(renderer=renderer, **kwargs)
        # self.shadow_vao = self.get_shadow_vao()

    def on_init(self):
        self.shader_program['m_view_light'].write(self.renderer.light.m_view_light)
        # depth texture
        self.shader_program['shadowMap'] = 32
        # Note that we use th same texture for objects
        self.depth_texture.use(location=32)

        # shadows
        # self.shadow_shader_program['m_proj'].write(self.camera.m_proj)
        # self.shadow_shader_program['m_view_light'].write(self.renderer.light.m_view_light)
        # self.shadow_shader_program['m_model'].write(self.m_model)

        self.shader_program['u_texture_0'] = self.tex_id
        self.texture.use(location=self.tex_id)
        self.shader_program['m_proj'].write(self.camera.m_proj)
        self.shader_program['m_model'].write(self.m_model)
        self.shader_program['m_view'].write(self.camera.m_view)
        self.shader_program['camPos'].write(self.camera.eye)
        self.shader_program['repeat'].write(glm.vec2(self.repeat))
        self.shader_program['light.position'].write(self.renderer.light.position)
        self.shader_program['light.Ia'].write(self.renderer.light.Ia)
        self.shader_program['light.Id'].write(self.renderer.light.Id)
        self.shader_program['light.Is'].write(self.renderer.light.Is)

    # def get_shadow_vao(self):
    #     """Get vertex array object for shadows"""
    #     if self.custom_vbo_format:
    #         vbo_format = self.custom_vbo_format
    #     else:
    #         vbo_format = self.vbo_format
    #     if self.custom_vbo_attribs:
    #         vbo_attribs = self.custom_vbo_attribs
    #     else:
    #         vbo_attribs = self.vbo_attribs
    #     # TODO: Do we really need normals, tangents etc. for the shadow shader
    #     vao = self.ctx.vertex_array(
    #         self.shadow_shader_program,
    #         [(self.vbo, vbo_format, *vbo_attribs)])
    #     return vao
    #
    # def update_shadow(self):
    #     self.shadow_shader_program['m_model'].write(self.m_model)
    #
    # def render_shadow(self):
    #     self.update_shadow()
    #     self.shadow_vao.render()

    def get_vertex_data(self):
        size_x, size_y = self.size_xy
        vertices = [(-size_x / 2, -size_y / 2, 0.),
                    (-size_x / 2, size_y / 2, 0.),
                    (size_x / 2, size_y / 2, 0.),
                    (size_x / 2, -size_y / 2, 0.)]
        # vertices = [(0., 0., 0.),
        #             (0., size_y, 0.),
        #             (size_x, size_y, 0.),
        #             (size_x, 0., 0.)]
        tri_indices = [(0, 1, 3), (3, 1, 2)]
        vertex_data = self.get_data(vertices, tri_indices)
        tex_coord = [(0, 0), (0, 1), (1, 1), (1, 0)]
        tex_coord_indices = [(0, 1, 3), (3, 1, 2)]
        tex_coord_data = self.get_data(tex_coord, tex_coord_indices)
        vertex_data = np.hstack([tex_coord_data, vertex_data])
        return vertex_data

    @staticmethod
    def get_data(vertices, indices):
        data = [vertices[ind] for triangle in indices for ind in triangle]
        return np.array(data, dtype='f4')

    @property
    def size_xy(self) -> tuple[float, float]:
        return self.size

    def update_camera(self):
        self.shader_program['m_proj'].write(self.camera.m_proj)
        self.shader_program['m_view'].write(self.camera.m_view)
        self.shader_program['camPos'].write(self.camera.eye)

    def destroy(self):
        self.vbo.release()
        self.shader_program.release()
        self.vao.release()
