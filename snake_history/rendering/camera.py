import glm
import pygame as pg

FOV = 50  # deg
NEAR = 0.1
# TODO: FAR distance must be set to relatively large value, if we use scaling
#  Should eventually be calculated automatically depending on screen world size
FAR = 2000
SPEED = 0.05
SENSITIVITY = 0.04


class Camera3d:
    def __init__(self, renderer, eye=(0, 0, 4), yaw=-90, pitch=0, up=(0, 1, 0), look_at=(0, 0, 0),
                 fov=FOV, aspect_ratio=None):
        self.renderer = renderer
        if aspect_ratio:
            self.aspect_ratio = aspect_ratio
        else:
            self.aspect_ratio = renderer.size[0] / renderer.size[1]
        self.eye = glm.vec3(eye)
        self.up = glm.vec3(up)
        self.right = glm.vec3(1, 0, 0)
        self.fov = fov
        self.look_at = glm.vec3(look_at)
        self.yaw = yaw
        self.pitch = pitch
        # view matrix
        self.m_view = self.get_view_matrix()
        # projection matrix
        self.m_proj = self.get_projection_matrix()

    def rotate(self):
        rel_x, rel_y = pg.mouse.get_rel()
        self.yaw += rel_x * SENSITIVITY
        self.pitch -= rel_y * SENSITIVITY
        self.pitch = max(-89, min(89, self.pitch))

    def update_camera_vectors(self):
        yaw, pitch = glm.radians(self.yaw), glm.radians(self.pitch)

        self.look_at.x = glm.cos(yaw) * glm.cos(pitch)
        self.look_at.y = glm.sin(pitch)
        self.look_at.z = glm.sin(yaw) * glm.cos(pitch)

        self.look_at = glm.normalize(self.look_at)
        self.right = glm.normalize(glm.cross(self.look_at, glm.vec3(0, 1, 0)))
        self.up = glm.normalize(glm.cross(self.right, self.look_at))

    def update(self):
        self.move()
        self.rotate()
        self.update_camera_vectors()
        self.m_view = self.get_view_matrix()

    def move(self):
        velocity = SPEED * self.renderer.delta_time
        keys = pg.key.get_pressed()
        if keys[pg.K_w]:
            self.eye += self.look_at * velocity
        if keys[pg.K_s]:
            self.eye -= self.look_at * velocity
        if keys[pg.K_a]:
            self.eye -= self.right * velocity
        if keys[pg.K_d]:
            self.eye += self.right * velocity
        if keys[pg.K_q]:
            self.eye += self.up * velocity
        if keys[pg.K_e]:
            self.eye -= self.up * velocity

    def get_view_matrix(self):
        # eye, center, up
        return glm.lookAt(self.eye, self.look_at, self.up)

    def get_projection_matrix(self):
        return glm.perspective(glm.radians(self.fov), self.aspect_ratio, NEAR, FAR)

    def align_to_2d_view(self, width, height):
        self.m_proj = glm.ortho(0, width, height, 0., -1., 1.)
