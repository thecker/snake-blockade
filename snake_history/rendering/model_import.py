"""Importers for 3D models"""

import abc
import struct
import numpy as np
import pywavefront
from pygltflib import GLTF2


class ModelImporter:

    @staticmethod
    @abc.abstractmethod
    def import_model(filename) -> np.array:
        """Import the model"""


class WavefrontOBJImporter(ModelImporter):

    """Wavefront OBJ importer

    Note:
        The UV map's y-coordinates are inverted here
    """
    @staticmethod
    def import_model(filename) -> np.array:
        objs = pywavefront.Wavefront(filename, cache=True, parse=True)
        obj = objs.materials.popitem()[1]
        vertex_data = obj.vertices
        vertex_data = np.array(vertex_data, dtype='f4')
        return vertex_data


class GLTFImporter(ModelImporter):

    """GLTF2 importer"""

    @staticmethod
    def import_model(filename, load_tangent=False) -> np.array:
        # TODO: Probably we can just pass the buffers without any numpy conversion...
        #  Although I think we need to re-pack data into certain shape
        gltf = GLTF2().load(filename)

        binary_blob = gltf.binary_blob()

        # TODO: Not sure, how we know the data type, but in this case we need to set it to uint16
        #  This does not seem to be a generic solution as it would limit the number of vertices to 64k
        triangles_accessor = gltf.accessors[gltf.meshes[0].primitives[0].indices]
        triangles_buffer_view = gltf.bufferViews[triangles_accessor.bufferView]
        triangle_vertex_indices = np.frombuffer(
            binary_blob[triangles_buffer_view.byteOffset + triangles_accessor.byteOffset:
                        triangles_buffer_view.byteOffset + triangles_buffer_view.byteLength],
            dtype="uint16",
            count=triangles_accessor.count,
        )

        position_accessor = gltf.accessors[gltf.meshes[0].primitives[0].attributes.POSITION]
        position_buffer_view = gltf.bufferViews[position_accessor.bufferView]
        position = np.frombuffer(
            binary_blob[position_buffer_view.byteOffset + position_accessor.byteOffset:
                        position_buffer_view.byteOffset + position_buffer_view.byteLength],
            dtype="float32",
            count=position_accessor.count * 3,
        ).reshape((-1, 3))
        
        texcoord_0_accessor = gltf.accessors[gltf.meshes[0].primitives[0].attributes.TEXCOORD_0]
        texcoord_0_buffer_view = gltf.bufferViews[texcoord_0_accessor.bufferView]
        texcoord_0 = np.frombuffer(
            binary_blob[texcoord_0_buffer_view.byteOffset + texcoord_0_accessor.byteOffset:
                        texcoord_0_buffer_view.byteOffset + texcoord_0_buffer_view.byteLength],
            dtype="float32",
            count=texcoord_0_accessor.count * 2,
        ).reshape((-1, 2))
        
        normal_accessor = gltf.accessors[gltf.meshes[0].primitives[0].attributes.NORMAL]
        normal_buffer_view = gltf.bufferViews[normal_accessor.bufferView]
        normal = np.frombuffer(
            binary_blob[normal_buffer_view.byteOffset + normal_accessor.byteOffset:
                        normal_buffer_view.byteOffset + normal_buffer_view.byteLength],
            dtype="float32",
            count=normal_accessor.count * 3,
        ).reshape((-1, 3))
        
        if load_tangent:
            tangent_accessor = gltf.accessors[gltf.meshes[0].primitives[0].attributes.TANGENT]
            tangent_buffer_view = gltf.bufferViews[tangent_accessor.bufferView]
            tangent = np.frombuffer(
                binary_blob[tangent_buffer_view.byteOffset + tangent_accessor.byteOffset:
                            tangent_buffer_view.byteOffset + tangent_buffer_view.byteLength],
                dtype="float32",
                count=tangent_accessor.count * 4,
            ).reshape((-1, 4))

        # Order defined for renderer - 'texcoord_0', 'normal', 'position', 'tangent'
        per_vertex_data = []
        for i, vertex_pos in enumerate(position):
            vertex_i_data = list(texcoord_0[i]) + list(normal[i]) + list(vertex_pos)
            if load_tangent:
                vertex_i_data = vertex_i_data + list(tangent[i])
            per_vertex_data.append(vertex_i_data)

        vertex_data = []
        for index in triangle_vertex_indices:
            vertex_data.append(per_vertex_data[index])

        vertex_data = np.array(vertex_data).flatten()

        return vertex_data



