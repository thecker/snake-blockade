"""Base classes for renderers"""

import abc

import pygame as pg


class Renderer(abc.ABC):

    def __init__(self, engine, fps=30, screen_width=None, screen_height=None,
                 windowed=False, display=0, use_overlay=False):
        self.display_width, self.display_height = pg.display.get_desktop_sizes()[display]
        if not screen_width or not screen_height:
            screen_width, screen_height = pg.display.get_desktop_sizes()[display]
        print(f'Initialized on display of size: {self.display_width} x {self.display_height}')
        self.engine = engine
        self.engine.add_renderer(self)
        self.fps = fps
        self.size = screen_width, screen_height
        self.windowed = windowed
        self.display = display
        self.use_overlay = use_overlay
        self.fpsController = pg.time.Clock()

    @abc.abstractmethod
    def gameOver(self):
        """Show game over screen"""

    @abc.abstractmethod
    def reset(self):
        """Reset the renderer to current game style"""

    @abc.abstractmethod
    def render(self):
        """Render the current frame to screen"""

    @abc.abstractmethod
    def show_image(self, image_filename):
        """Blit and image onto the screen"""
    