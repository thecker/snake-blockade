#version 330 core

layout (location = 0) in vec2 in_texcoord_0;
layout (location = 1) in vec3 in_normal;
layout (location = 2) in vec3 in_position;
layout (location = 3) in vec4 in_tangent;

out vec2 uv_0;
out vec3 fragPos;
out mat3 TBN;
out vec4 shadowCoord;

uniform mat4 m_proj;
uniform mat4 m_view;
uniform mat4 m_view_light;
uniform mat4 m_model;


mat4 m_shadow_bias = mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0
);

void main() {
    uv_0 = in_texcoord_0;
    fragPos = vec3(m_model * vec4(in_position, 1.0));
    gl_Position = m_proj * m_view * m_model * vec4(in_position, 1.0);

    vec3 tangent = in_tangent.xyz;
    vec3 bitangent = cross(in_normal, tangent) * in_tangent.w;

    vec3 T = normalize(vec3(m_model * vec4(tangent, 0.0)));
    vec3 B = normalize(vec3(m_model * vec4(bitangent, 0.0)));
    vec3 N = normalize(vec3(m_model * vec4(in_normal, 0.0)));
    TBN = mat3(T, B, N);

    mat4 shadowMVP = m_proj * m_view_light * m_model;
    shadowCoord = m_shadow_bias * shadowMVP * vec4(in_position, 1.0);
    shadowCoord.z -= 0.0005;
}