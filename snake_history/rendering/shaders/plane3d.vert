#version 330 core
layout (location = 0) in vec2 in_texcoord_0;
layout (location = 1) in vec3 in_position;

out vec2 uv_0;
out vec4 shadowCoord;
out vec3 fragPos;

uniform mat4 m_proj;
uniform mat4 m_model;
uniform mat4 m_view;
uniform mat4 m_view_light;
uniform vec2 repeat;

mat4 m_shadow_bias = mat4(
    0.5, 0.0, 0.0, 0.0,
    0.0, 0.5, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.5, 0.5, 0.5, 1.0
);

void main()
{
    uv_0 = vec2(in_texcoord_0.x * repeat.x, in_texcoord_0.y * repeat.y);
    gl_Position = m_proj * m_view * m_model * vec4(in_position, 1.0);
    fragPos = vec3(m_model * vec4(in_position, 1.0));

    mat4 shadowMVP = m_proj * m_view_light * m_model;
    shadowCoord = m_shadow_bias * shadowMVP * vec4(in_position, 1.0);
    shadowCoord.z -= 0.0005;
}
