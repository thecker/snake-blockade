#version 330 core
layout (location = 0) in vec2 in_texcoord_0;
layout (location = 1) in vec2 in_position;

out vec2 uv_0;

uniform mat4 m_proj;
uniform mat4 m_model;

void main()
{
    uv_0 = in_texcoord_0;
    gl_Position = m_proj * m_model *  vec4(in_position.xy, 0.0, 1.0);
}
