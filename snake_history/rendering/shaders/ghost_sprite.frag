#version 330 core

layout (location = 0) out vec4 fragColor;

in vec2 uv_0;

uniform sampler2D u_texture_0;
uniform float u_alpha;

void main()
{   
    // Simple texture mapping with definable alpha factor
    // TODO: We can probably just extend the normal sprite shader with this
    //  and do not need a separate shader for this
    vec4 color = vec4(1.0f, 1.0f, 1.0f, u_alpha) * texture(u_texture_0, uv_0).rgba;
    fragColor = color;
}  
