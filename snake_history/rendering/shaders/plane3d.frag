#version 330 core

layout (location = 0) out vec4 fragColor;

in vec2 uv_0;
in vec4 shadowCoord;
in vec3 fragPos;

struct Light {
    vec3 position;
    vec3 Ia;
    vec3 Id;
    vec3 Is;
};

uniform Light light;
uniform vec3 camPos;
uniform sampler2D u_texture_0;
 uniform sampler2DShadow shadowMap;

float getShadow() {
    float shadow = textureProj(shadowMap, shadowCoord);
    return shadow;
}

vec3 getLight(vec3 color) {
    vec3 Normal = vec3(0.0, 1.0, 0.0);

    // ambient light
    vec3 ambient = light.Ia;

    // diffuse light
    vec3 lightDir = normalize(light.position - fragPos);
    float diff = max(0, dot(lightDir, Normal));
    vec3 diffuse = diff * light.Id;

    // specular light
    vec3 viewDir = normalize(camPos - fragPos);
    vec3 reflectDir = reflect(-lightDir, Normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0), 32);
    vec3 specular = spec * light.Is;

    // shadow
    float shadow = getShadow();
    // float shadow = getSoftShadowX16();

    return color * (ambient + (diffuse + specular) * shadow);

}

void main()
{
    float gamma = 2.2;
    vec3 color = texture(u_texture_0, uv_0).rgb;
    // vec3 color = vec3(1.0, 0.0, 0.0);
    color = pow(color, vec3(gamma));

    color = getLight(color);

    color = pow(color, 1 / vec3(gamma));
    fragColor = vec4(color, 1.0);
}
