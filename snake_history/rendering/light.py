import glm


class Light:
    def __init__(self, position=(50, 50, -10), ambient=0.06, diffuse=0.8, specular=1.0, color=(1, 1, 1)):
        self.position = glm.vec3(position)
        self.color = glm.vec3(color)
        self.direction = glm.vec3(0, 0, 0)
        # components
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        # view matrix
        self.m_view_light = self.get_view_matrix()

    @property
    def Ia(self):
        """Ambient intensity"""
        return self.ambient * self.color

    @property
    def Id(self):
        """Diffuse intensity"""
        return self.diffuse * self.color

    @property
    def Is(self):
        """Specular intensity"""
        return self.specular * self.color

    def get_view_matrix(self):
        return glm.lookAt(self.position, self.direction, glm.vec3(0, 1, 0))