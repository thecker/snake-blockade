"""Classes & helper functions for graphic effects"""

import time
import pygame


class GhostFrameController:

    def __init__(self, game_frame_rate, ghosting_time=1.0, strength=0.5):
        self.strength = strength
        self.game_frame_rate = game_frame_rate
        self.ghosting_time = ghosting_time
        self.ghost_frames = []
        self.ghost_frame_times = []
        self.active = True

    def set_active(self):
        self.active = True

    def set_inactive(self):
        self.active = False

    @property
    def no_frames(self)->int:
        return int(self.game_frame_rate * self.ghosting_time)

    def add_frame(self, frame:pygame.Surface):
        frame.set_colorkey(pygame.Color('black'))
        self.ghost_frames.append(frame)
        self.ghost_frame_times.append(time.time())
        if len(self.ghost_frames) > self.no_frames:
            frame = self.ghost_frames.pop(0)
            del frame
            self.ghost_frame_times.pop(0)

    def clear_frames(self):
        no_frames = len(self.ghost_frames)
        for i in range(no_frames):
            frame = self.ghost_frames.pop(0)
            del frame
        self.ghost_frame_times = []

    def draw_frames(self, surface:pygame.Surface):
        cur_time = time.time()
        for i in range(len(self.ghost_frames)-1, 0, -1):
            frame = self.ghost_frames[i]
            frame_time = self.ghost_frame_times[i]
            frame_age = cur_time - frame_time
            alpha = int((1 - frame_age / self.ghosting_time) * 
                        255 * self.strength)
            frame.set_alpha(alpha)
            surface.blit(source=frame, dest=(0, 0))

