"""Graphics and game style management"""

import pygame
from pygame.color import Color


# Default Colors
red = Color('red')
black = Color('black')
white = Color('white')


class GenericBlock(pygame.sprite.Sprite):

    def __init__(self, image, rotate_images=True):
        pygame.sprite.Sprite.__init__(self)
        self.image = image
        self.rect = self.image.get_rect()
        self.rotate_images = rotate_images
        self.create_rotated_images()

    def create_rotated_images(self):
        self.image_right = self.image
        if self.rotate_images:
            self.image_left = pygame.transform.rotate(self.image, 180.)
            self.image_up = pygame.transform.rotate(self.image, 90.)
            self.image_down = pygame.transform.rotate(self.image, 270.)
        else:
            self.image_left = self.image
            self.image_down = self.image
            self.image_up = self.image

    def set_right(self):
        self.image = self.image_right

    def set_left(self):
        self.image = self.image_left

    def set_up(self):
        self.image = self.image_up
    
    def set_down(self):
        self.image = self.image_down


class Block(GenericBlock):

    # Constructor. Pass in the color of the block,
    # and its x and y position
    def __init__(self, color, width, height):
        self.color = color
        self.width = width
        self.height = height
        # Create an image of the block, and fill it with a color.
        # This could also be an image loaded from the disk.
        image = pygame.Surface([width, height])
        image.fill(color)
        super().__init__(image=image)

    def copy(self):
        return Block(color=self.color, width=self.width, height=self.height)


class ImageBlock(GenericBlock):

    def __init__(self, image_filename, **kwargs):
        self.image_filename = image_filename
        image = pygame.image.load(image_filename)
        super().__init__(image=image, **kwargs)

    def copy(self):
        return ImageBlock(image_filename=self.image_filename)


class GameStyle:
    # TODO: Should probably split this into 2D and 3D game styles...
    # TODO: Width and height are actually "playfield" block counts - resolution is display resolution
    def __init__(self, bg_color, snake_color=None, food_color=None, 
                 snake_shape=None, snake_head=None, rotate_head=False,
                 food_shape=None, width=40, height=30, block_size_x=16,
                 block_size_y=16,
                 resolution=(640, 480), aspect_ratio=None, font_color=pygame.Color('black'), name=None,
                 floor_tile=None, wall_tile=None, wall_corner_tile=None,
                 ghosting_time=None, ghosting_strength=None,
                 overlay=None, use_3d=False, normal_mapping=False,
                 head_model=None, body_model=None, food_model=None
                 ):
        self.bg_color = bg_color
        self.snake_color = snake_color
        self.food_color = food_color
        self.snake_shape = snake_shape
        self.snake_head = snake_head
        self.rotate_head = rotate_head
        self.food_shape = food_shape
        self.width = width
        self.height = height
        self.block_size_x = block_size_x
        self.block_size_y = block_size_y
        self.font_color = font_color
        self.name = name
        self.floor_tile = floor_tile
        self.wall_tile = wall_tile
        self.wall_corner_tile = wall_corner_tile
        self.ghosting_time = ghosting_time
        self.ghosting_strength = ghosting_strength
        self.overlay = overlay
        self.use_3d = use_3d
        self.normal_mapping = normal_mapping
        self.body_model = body_model
        self.head_model = head_model
        self.food_model = food_model
        if not aspect_ratio:
            aspect_ratio = 4/3
        else:
            size_x, size_y = aspect_ratio.split(':')
            aspect_ratio = float(size_x) / float(size_y)
        self.aspect_ratio = aspect_ratio
        self.resolution = resolution

    @property
    def playfield_range(self):
        """Playfield range in screen coordinates"""
        x_min = self.block_size_x
        x_max = self.width - self.block_size_x
        # 2 Blocks for score + 1 block for wall
        y_min = 3 * self.block_size_y
        y_max = self.height - self.block_size_y
        return (x_min, x_max, y_min, y_max)

    @property
    def playfield_block_size(self):
        block_x_min, block_x_max, block_y_min, block_y_max = \
                self.playfield_block_range
        return (block_x_max - block_x_min), (block_y_max - block_y_min)

    @property
    def screen_block_size(self):
        size_x = int(round(self.width / self.block_size_x + 0.1))
        size_y = int(round(self.height / self.block_size_y + 0.1))
        return (size_x, size_y)

    @property
    def playfield_block_range(self):
        """Playfield range in block IDs - 0 indexed"""
        x_min = 1
        x_max = self.width - 2
        y_min = 3
        y_max = self.height - 2
        return (x_min, x_max, y_min, y_max)
