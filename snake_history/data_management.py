"""Data management classes & helpers for loading assets etc."""

import json
from pathlib import Path

from pygame import Color
from pygame.image import load
from snake_history.graphics import GameStyle, ImageBlock, Block


class GameDataManager:

    def __init__(self, filename, data_folder, images_filename=None):
        self.filename = filename
        self.images_filename = images_filename
        self.data_folder = data_folder
        self.image_data = None
        self.game_styles:list[GameStyle]
        self.image_idx = -1
        self.style_idx = -1
        self.score = 0
        self.style_scores = []
        self.image_scores = []
        self.active_style = None
        self.active_image = None
        self.active_caption = None
        self._load_data()

    def _load_data(self):
        sprites_folder = self.data_folder.joinpath('sprites')
        with open(self.filename) as file:
            history_data = json.loads(file.read())
        if self.images_filename:
            with open(self.images_filename) as file:
                images_data = json.loads(file.read())
            self.image_data = images_data['images']
            for image_data in self.image_data:
                self.image_scores.append(image_data['score'])
        else:
            self.image_data = []
        epoche_data = history_data['epoches']
        game_styles = []
        for style_data in epoche_data:
            name = style_data['name']
            rotate_head = style_data['rotate_head']
            width = style_data['width']
            height = style_data['height']
            block_size_x = style_data['block_size_x']
            block_size_y = style_data['block_size_y']
            font_color_str = style_data['font_color']
            font_color = Color(font_color_str)
            self.style_scores.append(style_data['score'])
            bg_color_str = style_data['bg_color']
            bg_color = Color(bg_color_str)
            snake_img_filename = style_data.get('snake_image')
            snake_head_filename = style_data.get('snake_head')
            food_img_filename = style_data.get('food_image')
            floor_image_str = style_data.get('floor_image')
            wall_image_str = style_data.get('wall_image')
            wall_corner_img_str = style_data.get('wall_corner_image')
            ghosting_time = style_data.get('ghosting_time')
            ghosting_strength = style_data.get('ghosting_strength')
            overlay_filename = style_data.get('overlay')
            rotate_head = style_data.get('rotate_head')
            aspect_ratio = style_data.get('aspect_ratio')
            # Following is 3d only stuff
            use_3d = style_data.get('use_3d')
            if use_3d:
                resolution = style_data.get('resolution')
                if resolution:
                    resolution = tuple(resolution)
                else:
                    resolution = (640, 480)
            else:
                resolution = (width * block_size_x, height * block_size_y)
            normal_mapping = style_data.get('normal_mapping')
            head_model = style_data.get('head_model')
            body_model = style_data.get('body_model')
            food_model = style_data.get('food_model')
            game_style = GameStyle(
                    bg_color=bg_color,
                    snake_shape=snake_img_filename, snake_head=snake_head_filename,
                    rotate_head=rotate_head,
                    food_shape=food_img_filename, floor_tile=floor_image_str,
                    wall_tile=wall_image_str, wall_corner_tile=wall_corner_img_str,
                    width=width, height=height, block_size_x=block_size_x,
                    block_size_y=block_size_y, resolution=resolution,
                    aspect_ratio=aspect_ratio, font_color=font_color,
                    name=name, ghosting_time=ghosting_time, 
                    ghosting_strength=ghosting_strength,
                    overlay=overlay_filename, use_3d=use_3d, normal_mapping=normal_mapping,
                    head_model=head_model, body_model=body_model, food_model=food_model
                )
            game_styles.append(game_style)
        self.game_styles = game_styles

    @property
    def number_images(self)->int:
        return len(self.image_data)

    @property
    def number_styles(self)->int:
        return len(self.game_styles)

    def get_next_image_data(self)->tuple[Path, str]:
        if self.image_idx < self.number_images - 1:
            self.image_idx += 1
        image_filename = self.image_data[self.image_idx]['filename']
        caption = self.image_data[self.image_idx]['title']
        return image_filename, caption

    def get_next_game_style(self)->GameStyle:
        if self.style_idx < self.number_styles - 1:
            self.style_idx += 1
        game_style = self.game_styles[self.style_idx]
        self.active_style = game_style
        return game_style

    def add_score(self):
        #TODO: Should harmonize this - setting active content either here
        #  or in the get_next... method
        self.score += 1
        if self.score in self.style_scores:
            self.get_next_game_style()
        if self.score in self.image_scores:
            self.active_image, self.active_caption = self.get_next_image_data()
        else:
            self.active_image = None
            self.active_caption = None



