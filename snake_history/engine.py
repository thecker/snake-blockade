"""Game engine for controlling the snake game"""

import random
import sys
import time
from pathlib import Path

import pygame
from pygame.time import Clock


from snake_history.data_management import GameDataManager


class Actor:

    def __init__(self, x_pos=0, y_pos=0):
        self.x_pos = x_pos
        self.y_pos = y_pos

    @property
    def position(self):
        return (self.x_pos, self.y_pos)


class SnakePlayer(Actor):

    def __init__(self, size=3, direction='RIGHT', **kwargs):
        # TODO: `size` does not seem to have any effect...
        super().__init__(**kwargs)
        self.size = size
        self.direction = direction
        self.body = self.create_body()

    def create_body(self):
        return [[self.x_pos, self.y_pos],
                [self.x_pos - 1, self.y_pos],
                [self.x_pos - 2, self.y_pos]]

    def update(self, grow=False):
        self.body.insert(0, [self.x_pos, self.y_pos])
        if not grow:
            self.body.pop()
        

class GameEngine:

    def __init__(self, gdm:GameDataManager, game_fps=10, unlimited_lives=False, verbosity=0):
        # Pygame Init
        self.verbosity = verbosity
        init_status = pygame.init()
        if init_status[1] > 0:
            print("(!) Had {0} initialising errors, exiting... ".format(init_status[1]))
            sys.exit()
        else:
            print("(+) Pygame initialised successfully ")
        self.gdm = gdm
        self.game_fps = game_fps
        self.renderer = None
        self.player = self.create_player()
        self.food = self.spawn_food()
        self.score = 0
        self.game_style = self.gdm.get_next_game_style()
        self.last_game_style = self.game_style
        self.fpsController = Clock()
        self.unlimited_lives = unlimited_lives

    # TODO: Distinctions between 3d and 2d below are due to the
    #  fact that 2d mode uses screen space coordinates direction,
    #  while 3d has a play field definition independent of screen
    #  space. Should harmonize this and give 2d mode an abstract
    #  "world" space, which should be defined to match screen space.
    @property
    def block_x_min(self):
        if self.game_style.use_3d:
            block_x_min = 1
        else:
            # TODO: Harmonize 2D/3D logic
            block_x_min = self.game_style.playfield_block_range[0]
        return block_x_min

    @property
    def block_x_max(self):
        if self.game_style.use_3d:
            block_x_max = self.game_style.width
        else:
            # TODO: Harmonize 2D/3D logic
            block_x_max = self.game_style.playfield_block_range[1]
        return block_x_max

    @property
    def block_y_min(self):
        if self.game_style.use_3d:
            block_y_min = 1
        else:
            # TODO: Harmonize 2D/3D logic
            block_y_min = self.game_style.playfield_block_range[2]
        return block_y_min

    @property
    def block_y_max(self):
        if self.game_style.use_3d:
            block_y_max = self.game_style.height
        else:
            # TODO: Harmonize 2D/3D logic
            block_y_max = self.game_style.playfield_block_range[3]
        return block_y_max

    def create_player(self):
        return SnakePlayer(size=3, x_pos=10, y_pos=5)

    def spawn_food(self):
        return Actor(x_pos=20, y_pos=5)

    def add_renderer(self, renderer):
        self.renderer = renderer

    def check_events(self):
        changeto = ''
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT: # or event.key == pygame.K_d:
                    changeto = 'RIGHT'
                if event.key == pygame.K_LEFT:#  or event.key == pygame.K_a:
                    changeto = 'LEFT'
                if event.key == pygame.K_UP: # or event.key == pygame.K_w:
                    changeto = 'UP'
                if event.key == pygame.K_DOWN: # or event.key == pygame.K_s:
                    changeto = 'DOWN'
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
                if event.key == pygame.K_p or event.key == pygame.K_PAUSE:
                    self.pause_game()
        # Validate direction
        if changeto == 'RIGHT' and self.player.direction != 'LEFT':
            self.player.direction = changeto
        elif changeto == 'LEFT' and self.player.direction != 'RIGHT':
            self.player.direction = changeto
        elif changeto == 'UP' and self.player.direction != 'DOWN':
            self.player.direction = changeto
        elif changeto == 'DOWN' and self.player.direction != 'UP':
            self.player.direction = changeto

    def update_player(self):
        # Update snake position
        direction = self.player.direction
        player = self.player
        if direction == 'RIGHT':
            player.x_pos += 1
        elif direction == 'LEFT':
            player.x_pos -= 1
        elif direction == 'DOWN':
            player.y_pos += 1
        elif direction == 'UP':
            player.y_pos -= 1

        if self.verbosity > 0:
            print(f'Player pos. (blocks): {player.position}')

        # Snake body mechanism
        if player.position != self.food.position:
            player.update(grow=False)
        else:
            player.update(grow=True)

    def advance_level(self):
        player = self.player
        game_style = self.game_style
        direction = player.direction
        # Need to recalculate the snake position on style change to avoid
        # that we jump or even leave the valid play field range
        # print('Old pos.:', snakePos)
        no_blocks_x_old, no_blocks_y_old = \
                self.last_game_style.playfield_block_size
        no_blocks_x_new, no_blocks_y_new = \
                self.game_style.playfield_block_size
        player.x_pos = \
                int(round(player.x_pos / no_blocks_x_old * no_blocks_x_new))
        player.y_pos = \
                int(round(player.y_pos / no_blocks_y_old * no_blocks_y_new))
        # TODO: Seems like there is still a bug with the pos 0 and 1 ending
        #  up on the same block after transformation
        for i in range(len(player.body)):
            player.body[i][0] = \
                    int(round(player.body[i][0] / no_blocks_x_old * no_blocks_x_new))
            player.body[i][1] = \
                    int(round(player.body[i][1] / no_blocks_y_old * no_blocks_y_new))
        # Make sure, that snakePos differs from body parts
        if player.position == tuple(player.body[0]):
            if direction == 'RIGHT':
                player.x_pos += 1
            elif direction == 'LEFT':
                player.x_pos -= 1
            elif direction == 'DOWN':
                player.y_pos += 1
            elif direction == 'UP':
                player.y_pos -= 1
        self.last_game_style = game_style
        if self.verbosity > 1:
            print(f'Playfield block range: x=({self.block_x_min}, {self.block_x_max}) '
                  f'| y=({self.block_y_min}, {self.block_y_max})')

    def game_over(self):
        if self.unlimited_lives:
            time.sleep(4)
            self.player.direction = 'RIGHT'
            self.restart()
        else:
            self.renderer.gameOver()
            time.sleep(4)
            pygame.quit()
            sys.exit()

    def restart(self):
        self.player = self.create_player()
        self.food = self.spawn_food()
        self.renderer.reset()

    def pause_game(self):
        continue_game = False
        self.renderer.show_centered_caption(text='Press any key to continue')
        while not continue_game:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    # Repost last keypress
                    pygame.event.post(event)
                    continue_game = True
                    break

    def run(self):
        if 'start_screen.png' in self.renderer.textures:
            self.renderer.show_image(image_filename='start_screen.png')
            # TODO: Should eventually update the pause_game method to only accept certain keys for continuing to remove
            #  some redundancies here...
            continue_game = False
            while not continue_game:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        sys.exit()
                    elif event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_SPACE:
                            continue_game = True
                            break
        background_music_file = Path('data/music/background_music.mp3')
        if background_music_file.exists():
            pygame.mixer.music.load(background_music_file)
            # Set to -1 for infinite loop
            pygame.mixer.music.play(-1)
        food_spawn = True
        # Make sure first style is loaded correctly
        self.renderer.reset()
        while True:
            game_frame_start = time.time()
            self.check_events()
            self.update_player()
            self.renderer.render()

            # Check player interactions
            # Caught the food
            if self.player.position == self.food.position:
                food_spawn = False
                self.score += 1
                # TODO: Should add more intuitive logic for advancing levels
                self.gdm.add_score()
                filename = self.gdm.active_image
                caption = self.gdm.active_caption
                if filename:
                    self.renderer.show_image(image_filename=filename, caption=caption)
                    continue_game = False
                    while not continue_game:
                        for event in pygame.event.get():
                            if event.type == pygame.QUIT:
                                pygame.quit()
                                sys.exit()
                            elif event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_SPACE:
                                    self.renderer.hide_current_image()
                                    self.renderer.render()
                                    self.pause_game()
                                    continue_game = True
                                    break
                self.game_style = self.gdm.active_style
                # Advance one level, if new game style
                if self.game_style != self.last_game_style:
                    self.advance_level()
                    self.renderer.reset()
            if not food_spawn:
                self.food.x_pos = random.randrange(self.block_x_min, self.block_x_max) 
                self.food.y_pos = random.randrange(self.block_y_min, self.block_y_max)
                if self.verbosity > 0:
                    print(f'Food pos. (blocks): {self.food.position}')
                food_spawn = True

            # Bounds
            # Note, that the snake sprite is usually 2 time the block size and the
            # snake position is the upper left corner of the snake sprite
            # Hence we need to substract the block size from the sprite size
            if self.player.x_pos > (self.block_x_max - 1) \
                    or self.player.x_pos < self.block_x_min:
                if self.verbosity > 0:
                    print(f'Snake hit right/left wall at {self.player.position}!')
                self.renderer.show_centered_caption(text='Snake hit wall!')
                self.game_over()
            if self.player.y_pos > (self.block_y_max - 1) \
                    or self.player.y_pos < self.block_y_min:
                if self.verbosity > 0:
                    print(f'Snake hit top/bottom wall at {self.player.position}!')
                self.renderer.show_centered_caption(text='Snake hit wall!')
                self.game_over()

            # Self hit
            for block in self.player.body[1:]:
                if self.player.position == tuple(block):
                    if self.verbosity > 0:
                        print(f'Snake bit itself at {self.player.position}!')
                    self.renderer.show_centered_caption(text='Snake bit itself!')
                    self.game_over()
            if self.verbosity > 1:
                print(f'Game frame took {time.time() - game_frame_start} s.')
            self.fpsController.tick(self.game_fps)
