# Historic Snake

Snake game, which cycles through historic PC graphics modes.

Game engine originally based on [Snake Blockade - The classic snake game developed using PyGame](https://www.pygame.org/project/3536)

OpenGL renderer originally based on [OpenGL Graphics Engine in Python ( Pygame, ModernGL )](https://github.com/StanislavPetrovV/3D-Graphics-Engine#3d-graphics-engine)

## Installation

Install dependencies with pip `pip install -r requirements.txt`

## Running the game

Just run `python snake_game.py` or type `python snake_game.py -h` to see available command line option.